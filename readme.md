# Telegramify
a server thing to send your data to your telegram account using a bot

## How to use
- install dependencies: `flask`, `requests`
- get a telegram bot token from `@BotFather`
- get your user id from `@JsonDumpBot`
- export these variables to your shell: `TG_BOT_TOKEN`, `TG_USER_ID`
- hash a password with SHA256 and export it for: `HASHED_PASSWORD`
- just run it and available on `127.0.0.1:5000`